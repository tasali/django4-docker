FROM python:3-alpine

RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN apk add --no-cache jpeg-dev zlib-dev uwsgi uwsgi-python3 bash
WORKDIR /django
ADD requirements.txt ./
RUN pip3 install -r requirements.txt
